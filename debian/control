Source: doomsday
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Michael Gilbert <mgilbert@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 12),
 python3:any,
 qtbase5-dev,
 zlib1g-dev,
 libassimp-dev,
 libfluidsynth-dev,
 libncurses-dev,
 libsdl2-mixer-dev,
 libx11-dev,
 libxrandr-dev,
 libxxf86vm-dev,
 libqt5opengl5-desktop-dev,
 libqt5x11extras5-dev,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libminizip-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.dengine.net
Vcs-Git: https://salsa.debian.org/games-team/deng.git
Vcs-Browser: https://salsa.debian.org/games-team/deng

Package: doomsday
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 doomsday-data (>= ${source:Version}), doomsday-data (<< ${source:Version}.1~),
 doomsday-common (>= ${source:Version}), doomsday-common (<< ${source:Version}.1~),
Recommends:
 fluid-soundfont-gm,
Provides:
 boom-engine,
 doom-engine,
 heretic-engine,
 hexen-engine,
Description: enhanced version of the legendary DOOM game
 The purpose of the Doomsday Engine project is to create versions of DOOM,
 Heretic and Hexen that feel the same as the original games but are implemented
 using modern techniques such as 3D graphics and client/server networking. A
 lot of emphasis is placed on good-looking graphics.
 .
 Doomsday Engine (or deng for short) requires an IWAD to play. You can install
 your commercial IWADs using game-data-packager.

Package: doomsday-server
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 doomsday-data (>= ${source:Version}), doomsday-data (<< ${source:Version}.1~),
 doomsday-common (>= ${source:Version}), doomsday-common (<< ${source:Version}.1~),
Replaces:
 doomsday (<= 1.14.5-2),
Breaks:
 doomsday (<= 1.14.5-2),
Description: enhanced version of the legendary DOOM game - server
 The purpose of the Doomsday Engine project is to create versions of DOOM,
 Heretic and Hexen that feel the same as the original games but are implemented
 using modern techniques such as 3D graphics and client/server networking. A
 lot of emphasis is placed on good-looking graphics.
 .
 This package contains the dedicated server.

Package: doomsday-common
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 doomsday (<= 1.14.5-2),
Breaks:
 doomsday (<= 1.14.5-2),
Description: enhanced version of the legendary DOOM game - common files
 The purpose of the Doomsday Engine project is to create versions of DOOM,
 Heretic and Hexen that feel the same as the original games but are implemented
 using modern techniques such as 3D graphics and client/server networking. A
 lot of emphasis is placed on good-looking graphics.
 .
 This package includes files common to both server and client packages.

Package: doomsday-data
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 freedoom | game-data-packager | boom-wad | doom-wad | heretic-wad | hexen-wad,
Replaces:
 doomsday (<= 1.14.5-2),
Breaks:
 doomsday (<= 1.14.5-2),
Description: enhanced version of the legendary DOOM game - data files
 The purpose of the Doomsday Engine project is to create versions of DOOM,
 Heretic and Hexen that feel the same as the original games but are implemented
 using modern techniques such as 3D graphics and client/server networking. A
 lot of emphasis is placed on good-looking graphics.
 .
 This package includes architecture-independent data files.
